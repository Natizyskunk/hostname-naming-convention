# Hostname naming convention
The correct way of using domains/hostnames.

- Need to start with a letter.
- Need to end with a letter or digit.
- Can have as interior characters only letters, digits, hyphen and period.
- Labels must be 63 characters or less.
- Only subdomains could start with digits.

Valid characters that can be used in a domain name are :
```
a-z (Letters).
0-9 (Numbers).
- (Hyphen) but not as a starting or ending character.
. (Period) as a separator for the textual portions of a domain name.
```
